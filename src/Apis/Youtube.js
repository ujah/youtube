import axios from 'axios';

const KEY = 'AIzaSyBC-lxwIzr4tE47HtwcqJWF0x3Uq6z74xs';

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params: {
        maxResults: 7,
        key: KEY
    }
});
//'AIzaSyBLbRLpX6RPDAR-MErz6ctVk-ImKFtU6wg'