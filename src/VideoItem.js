import React from 'react';

const VideoItem = (props) => {
    return (
        <div onClick={()=>{props.onVideoSelect(props.video)}} className="item">
            <img src={props.video.snippet.thumbnails.medium.url} alt='pics' className="ui image"/>
            <div className="content">
            <div className="header"> {props.video.snippet.title} </div>
            </div>
        </div>
    );
} 

export default VideoItem;