import React, { Component } from 'react';

class SearchBar extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            typing: '',
        SearchItem: ''
         };
         this.handleChange = this.handleChange.bind(this);
         this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange (e) {
        this.setState({
            typing: e.target.value
        });
    }

    handleSubmit = (e) => {

        e.preventDefault();

        this.props.onSubmit(this.state.typing)
        }
    
    render() {
        return ( 
            <div className="ui segment">
                <form className="ui form" onSubmit={this.handleSubmit}>
                    <div className="field">
                    <label>Video Search</label>
                    <input type="text" value={this.state.typing} onChange={this.handleChange} />
                    </div>
                </form>
            </div>
         );
    };
}
 
export default SearchBar;
